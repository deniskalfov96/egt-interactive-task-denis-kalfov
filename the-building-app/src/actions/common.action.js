import { SET_PAGE_TITLE, SET_FOOTER_MENU_ACTIVE_TAB } from './types';
import { FOOTER_PAGES } from '../constants'

export const setPageTitle = (pageName, pageUrl = null) => dispatch => {
    dispatch({
        type: SET_PAGE_TITLE,
        payload: pageName
    })
    dispatch(setFooterMenuActiveTab(pageUrl != null && pageName != pageUrl ? pageUrl : pageName));
}

export const setFooterMenuActiveTab = (pageName) => dispatch => {
    dispatch(setFooterMenuActiveTabValue(FOOTER_PAGES[pageName.replace(/\s+/g, '-').toLowerCase()]))
}

export const setFooterMenuActiveTabValue = (menuValue) => dispatch => {
    dispatch({
        type: SET_FOOTER_MENU_ACTIVE_TAB,
        payload: menuValue
    })
}