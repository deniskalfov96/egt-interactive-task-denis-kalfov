import React, { useState } from "react";
import { connect, useStore } from "react-redux";
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import Paper from '@mui/material/Paper';
import MapsHomeWorkIcon from '@mui/icons-material/MapsHomeWork';
import HomeIcon from '@mui/icons-material/Home';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import { useHistory } from "react-router-dom";
import { FOOTER_PAGES } from './constants';
import { setFooterMenuActiveTabValue } from './actions/common.action';

const Footer = (props) => {
    const state = useStore().getState();
    const history = useHistory();

    return (
        <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
            <BottomNavigation
                showLabels
                value={state.common.footerMenuActiveTab}
                onChange={(e, newValue) => {
                    props.setFooterMenuActiveTabValue(newValue)
                    history.push('/' + Object.keys(FOOTER_PAGES).find((v, k) => k == newValue))
                }}
            >
                <BottomNavigationAction label="Home" icon={<HomeIcon />} />
                <BottomNavigationAction label="Buildings" icon={<MapsHomeWorkIcon />} />
                <BottomNavigationAction label="About" icon={<AssignmentIndIcon />} />
            </BottomNavigation>
        </Paper>
    );
}

const mapStateToProps = (state, ownProps) => {
    return {
        footerActiveTab: state.common.footerActiveTab || {},
    }
}

export default connect(mapStateToProps, { setFooterMenuActiveTabValue })(Footer)