import { SET_PAGE_TITLE, SET_FOOTER_MENU_ACTIVE_TAB } from '../actions/types';

const initialState = {
    pageTitle: '',
    footerMenuActiveTab: 0
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_PAGE_TITLE:
            return {
                ...state,
                pageTitle: action.payload
            }
        case SET_FOOTER_MENU_ACTIVE_TAB:
            return {
                ...state,
                footerMenuActiveTab: action.payload
            }
        default:
            return state;
    }
}