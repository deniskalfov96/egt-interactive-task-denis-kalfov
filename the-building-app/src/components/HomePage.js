import React, { Component } from "react";
import { connect } from "react-redux";
import { setPageTitle } from '../actions/common.action';
import buildingImage from './../images/house-with-key.jpg';
import Button from '@mui/material/Button';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.setPageTitle('Home page', '');
    }

    render() {
        return (
            <div style={{ display: 'block', maxWidth: 500, margin: '0 auto', marginTop: 50, marginBottom: 100, textAlign: 'center' }}>

                <img src={buildingImage} style={{ maxWidth: 500, marginBottom: 25, borderRadius: 11 }} alt="Buildings" />

                This is the Home page, and this is your Home ^ !

                <p>I am glad, that only you have the key. Press the button below to start the journey.</p>

                <Button variant="text" onClick={() => this.props.history.push('/buildings')}>Go to Buildings</Button>

            </div>
        );
    }
}


const mapStateToProops = state => {
    return {
    };
};

export default connect(mapStateToProops, { setPageTitle })(HomePage);