import React, { Component } from "react";
import { connect } from "react-redux";
import { setPageTitle } from '../actions/common.action';

class AboutPage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setPageTitle('About');
    }

    render() {
        return (
            <div style={{ display: 'block', maxWidth: 1000, margin: '0 auto', marginTop: 50, marginBottom: 100, textAlign: 'center' }}>
                <h1>Imagine super fancy text here (:</h1>
                <br />
                <hr />
                <br />
                <h3>Made by: Denis Kalfov</h3>
            </div >
        );
    }
}

const mapStateToProops = state => {
    return {
    };
};

export default connect(mapStateToProops, { setPageTitle })(AboutPage);