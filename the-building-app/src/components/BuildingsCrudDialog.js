import React, { useState } from "react";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Snackbar from '@mui/material/Snackbar';

export default function BuildingsCrudDialog(props) {
    const [idError, setIdError] = React.useState(null)
    const [nameError, setNameError] = React.useState(null)
    const [areaError, setAreaError] = React.useState(null)

    const [response, setResponse] = React.useState(null)

    const getDefaultValue = (column) => {
        if (props.crudModalSelectedId && props.crudModalType == 'edit') {
            const editableRow = props.data.find(d => d.id == props.crudModalSelectedId);
            if (editableRow) {
                return editableRow[column];
            }
        }
        return ''
    }

    const [id, setIdValue] = React.useState(null);
    const [name, setNameValue] = React.useState(null);
    const [area, setAreaValue] = React.useState(null);
    const [location, setLocation] = React.useState(null);
    const [image, setImage] = React.useState(null);

    const setId = (id) => {
        if (id.length == 0) {
            setIdError('Id is required');
        } else if (id != props.crudModalSelectedId && props.data.find(d => d.id == id)) {
            setIdError('Id already used, try with another one');
        } else {
            setIdValue(id);
            setIdError(null);
        }
    }

    const setName = (name) => {
        if (name.length < 3) {
            setNameError('Name must be at least 3 characters long');
        } else {
            setNameError(null);
            setNameValue(name);
        }
    }

    const setArea = (area) => {
        if (area.length == 0) {
            setAreaError('Area is required');
        } else {
            setAreaError(null);
            setAreaValue(area);
        }
    }

    const submitTheForm = () => {
        let updatedData = props.data;

        const indexToModify = props.crudModalType == 'add' ? (props.data.length) : (props.data.findIndex(d => d.id == props.crudModalSelectedId));
        if (!updatedData[indexToModify]) {
            updatedData[indexToModify] = {}
        }
        if (props.crudModalType == 'add' || props.crudModalType == 'edit') {
            if (id) {
                updatedData[indexToModify].id = id;
            }
            if (name) {
                updatedData[indexToModify].name = name;
            }
            if (area) {
                updatedData[indexToModify].area = area;
            }
            if (location) {
                updatedData[indexToModify].location = location;
            }
            if (image) {
                updatedData[indexToModify].image = image;
            }
        } else if (props.crudModalType == 'delete') {
            updatedData.splice(parseInt(indexToModify), 1)
        }

        // Make the req
        var xhr = new XMLHttpRequest();
        xhr.open("POST", 'https://denis.tabl.bg/other/egt/update.php', true);

        //Send the proper header information along with the request
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () { // Call a function when the state changes.
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                setResponse(this.response);
                props.handleClose()
            }
        }
        xhr.send("data=" + JSON.stringify(updatedData));
    }

    return (
        <div>

            <Snackbar
                open={!!response}
                autoHideDuration={5000}
                onClose={() => setResponse('')}
                message={response}
            />

            <Dialog open={props.open} onClose={props.handleClose}>
                <DialogTitle>
                    {props.crudModalType == 'edit' || props.crudModalType == 'delete' ?
                        <>
                            {props.crudModalType == 'edit' ? 'Edit' : 'Delete'} Building #{props.crudModalSelectedId}
                        </>
                        :
                        <>Add new Building</>
                    }
                </DialogTitle>
                <DialogContent>

                    <DialogContentText>
                        {props.crudModalType == 'delete' ?
                            'Are you sure you want to delete the building ?' :
                            'Fill out the form and press the button to save/discard the changes'
                        }
                    </DialogContentText>

                    {props.crudModalType == 'add' || props.crudModalType == 'edit' ?
                        <div>
                            <p style={{ color: 'red', fontWeight: 'bold', margin: 0 }}>{idError || nameError || areaError}&nbsp;</p>

                            <TextField
                                autoFocus
                                margin="dense"
                                id="id"
                                name="id"
                                label="Id *"
                                type="text"
                                fullWidth
                                variant="standard"
                                defaultValue={getDefaultValue('id')}
                                onChange={(e) => setId(e.target.value)}
                            />

                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                name="name"
                                label="Name *"
                                type="text"
                                fullWidth
                                variant="standard"
                                defaultValue={getDefaultValue('name')}
                                onChange={(e) => setName(e.target.value)}
                            />

                            <TextField
                                autoFocus
                                margin="dense"
                                id="Area"
                                name="Area"
                                label="Area *"
                                type="text"
                                fullWidth
                                variant="standard"
                                defaultValue={getDefaultValue('area')}
                                onChange={(e) => setArea(e.target.value)}
                            />

                            <TextField
                                autoFocus
                                margin="dense"
                                id="Location"
                                name="Location"
                                label="Location"
                                type="text"
                                fullWidth
                                variant="standard"
                                defaultValue={getDefaultValue('location')}
                                onChange={(e) => setLocation(e.target.value)}
                            />

                            <TextField
                                autoFocus
                                margin="dense"
                                id="Image"
                                name="Image"
                                label="Image"
                                type="text"
                                fullWidth
                                variant="standard"
                                defaultValue={getDefaultValue('image')}
                                onChange={(e) => setImage(e.target.value)}
                            />
                        </div>
                        :
                        null
                    }

                </DialogContent>
                <DialogActions>
                    <Button onClick={props.handleClose}>Cancel</Button>
                    <Button onClick={props.handleClose} disabled={!!(idError || nameError || areaError)} onClick={() => submitTheForm()}>Submit</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
