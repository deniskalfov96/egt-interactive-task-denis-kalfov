import React, { Component } from "react";
import { connect } from "react-redux";
import { setPageTitle } from '../actions/common.action';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import BuildingsCrudDialog from "./BuildingsCrudDialog";
// let jsonData = require('./../data.json');

class BuildingsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jsonData: {},
            jsonDataInterval: null,
            crudModalOpened: false,
            crudModalType: '',
            crudModalSelectedId: '',
        };
    }

    fetchJsonData = () => {
        const xhr = new XMLHttpRequest();
        const url = 'https://denis.tabl.bg/other/egt/data.json';

        xhr.open('GET', url);
        xhr.responseType = 'json';

        // Update state only if there is data
        xhr.onreadystatechange = jsonData => { jsonData.target.response && Object.values(jsonData.target.response).length > 0 ? this.setState({ jsonData: jsonData.target.response }) : void (0) };
        xhr.send();

        // Not used in localhost, because of cors
        // fetch('https://denis.tabl.bg/other/egt/data.json')
        //     .then(function (response) {
        //          console.log('response.json()',response.json());
        //     })
        //     .then(function (jsonResponse) {
        //         // do something with jsonResponse
        //     });
    }

    componentDidMount() {
        this.props.setPageTitle('Welcome to Buildings', 'buildings');

        this.fetchJsonData();
        let jsonDataInterval = setInterval(() => {
            this.fetchJsonData();
        }, 5000)

        this.setState({ jsonDataInterval })
    }

    componentWillUnmount() {
        clearInterval(this.state.jsonDataInterval);
    }

    render() {
        return (<>

            <BuildingsCrudDialog
                data={this.state.jsonData}
                open={this.state.crudModalOpened}
                crudModalType={this.state.crudModalType}
                crudModalSelectedId={this.state.crudModalSelectedId}
                handleClickOpen={() => this.setState({ crudModalOpened: true })}
                handleClose={() => this.setState({ crudModalOpened: false })}
            />

            <TableContainer component={Paper} style={{ display: 'block', maxWidth: 1000, margin: '0 auto', marginTop: 50, marginBottom: 100, textAlign: 'center' }}>

                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell align="right">Name</TableCell>
                            <TableCell align="right">Area</TableCell>
                            <TableCell align="right">Location</TableCell>
                            <TableCell align="right">Image</TableCell>
                            <TableCell align="right">Acton
                                <Button variant="text"
                                    onClick={() => this.setState({
                                        crudModalOpened: true,
                                        crudModalType: 'add',
                                    })}>
                                    +
                                </Button>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Object.values(this.state.jsonData).map((row, k) => (
                            <TableRow
                                key={k}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.area}</TableCell>
                                <TableCell align="right">{row.location}</TableCell>
                                <TableCell align="right"><img src={row.image} style={{ maxWidth: 50 }} /></TableCell>
                                <TableCell align="right">
                                    <Button variant="text"
                                        onClick={() => this.setState({
                                            crudModalOpened: true,
                                            crudModalType: 'edit',
                                            crudModalSelectedId: row.id
                                        })}>
                                        Edit
                                    </Button>
                                    <Button variant="text"
                                        onClick={() => this.setState({
                                            crudModalOpened: true,
                                            crudModalType: 'delete',
                                            crudModalSelectedId: row.id
                                        })}>
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer >
        </>
        );
    }
}


const mapStateToProops = state => {

    return {
    };
};

export default connect(mapStateToProops, { setPageTitle })(BuildingsPage);