import React, { Component } from "react";
import Router from "./Router";
import Footer from "./Footer";
import Header from "./Header";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="page-container">
          <Header />
          <Router />
          <Footer/>
        </div>
      </React.Fragment >
    );
  }
}

export default App