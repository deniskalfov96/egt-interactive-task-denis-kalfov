import React from 'react';
import { Switch, Route } from 'react-router-dom';

import HomePage from './components/HomePage';
import BuildingsPage from './components/BuildingsPage';
import AboutPage from './components/AboutPage';

const Router = () => (
    <div>
        <Switch>
            <Route exact path='/buildings' component={BuildingsPage} />
            <Route exact path='/about' component={AboutPage} />
            <Route exact path='/' component={HomePage} />
        </Switch>
    </div>
)

export default Router;